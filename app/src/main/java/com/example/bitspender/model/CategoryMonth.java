package com.example.bitspender.model;

public class CategoryMonth {
    String name;
    int icon;
    float amount;
    int concept_type_fk;


    public CategoryMonth() {
    }

    public int getConcept_type_fk() {
        return concept_type_fk;
    }

    public void setConcept_type_fk(int concept_type_fk) {
        this.concept_type_fk = concept_type_fk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
