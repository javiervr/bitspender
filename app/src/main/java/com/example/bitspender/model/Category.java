package com.example.bitspender.model;

public class Category {
    int id;
    String name;
    int icono;
    boolean status;
    int concept_type_fk;

    public Category(int id, String name, int icono, boolean status,int concept_type_fk) {
        this.id = id;
        this.name = name;
        this.icono = icono;
        this.status = status;
        this.concept_type_fk=concept_type_fk;
    }

    public Category() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getConcept_type_fk() {
        return concept_type_fk;
    }

    public void setConcept_type_fk(int concept_type_fk) {
        this.concept_type_fk = concept_type_fk;
    }
}
