package com.example.bitspender.model;

public class Concept {
    int id;
    float amount;
    String description;
    String date;
    int category_fk;
    int concept_type_fk;

    public  Concept(){

    }

    public Concept(int id, float amount, String description, String date, int category_fk, int concept_type_fk) {
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.category_fk = category_fk;
        this.concept_type_fk = concept_type_fk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCategory_fk() {
        return category_fk;
    }

    public void setCategory_fk(int category_fk) {
        this.category_fk = category_fk;
    }

    public int getConcept_type_fk() {
        return concept_type_fk;
    }

    public void setConcept_type_fk(int concept_type_fk) {
        this.concept_type_fk = concept_type_fk;
    }
}
