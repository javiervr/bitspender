package com.example.bitspender.model;

public class HistoryModel {
    int id;
    float amount;
    String description;
    String date;
    String category;
    int categoryIcon;
    int concept_type_fk;
    public  HistoryModel(){

    }
    public HistoryModel(int id, float amount, String description, String date, String category, int categoryIcon, int concept_type_fk) {
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.category = category;
        this.categoryIcon = categoryIcon;
        this.concept_type_fk = concept_type_fk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(int categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public int getConcept_type_fk() {
        return concept_type_fk;
    }

    public void setConcept_type_fk(int concept_type_fk) {
        this.concept_type_fk = concept_type_fk;
    }
}
