package com.example.bitspender.model;


public class ChartModel {
    String label;
    float value;

    public ChartModel(){

    }
    public ChartModel(String label, float value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
