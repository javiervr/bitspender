package com.example.bitspender.model;

public class ConceptType {
    int id;
    String name;

    public ConceptType(){

    }

    public ConceptType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
