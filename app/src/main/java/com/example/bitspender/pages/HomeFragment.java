package com.example.bitspender.pages;


import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.bitspender.R;
import com.example.bitspender.activity.CategoryActivity;
import com.example.bitspender.adapter.CategoryMonthListAdapter;
import com.example.bitspender.controller.DatabaseHelper;
import com.example.bitspender.model.CategoryMonth;
import com.example.bitspender.utilities.Util;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    CardView mCardView;
    TextView mExpense,mIncome,mTotal;
    DatabaseHelper db;

    private RecyclerView mRecyclerViewIngress,mRecyclerViewExpensive;
    private RecyclerView.LayoutManager layoutManagerIngress,layoutManagerExpensive;
    private ArrayList<CategoryMonth> mCategoryMonthArrayListExpensive,mCategoryMonthArrayListIngress;
    private  RecyclerView.Adapter mAdapterIngress,mAdapterExpensive;
    private static HomeFragment instance = null;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        instance=this;
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        mCardView=view.findViewById(R.id.card_view);
        mExpense=view.findViewById(R.id.monthlyExpense);
        mIncome=view.findViewById(R.id.monthlyIncome);
        mTotal=view.findViewById(R.id.totalSaving);

        db=new DatabaseHelper(getContext());


        mRecyclerViewIngress =  view.findViewById(R.id.list_ingress);
        mRecyclerViewExpensive =  view.findViewById(R.id.list_expensive);

        layoutManagerIngress = new LinearLayoutManager(getContext());
        layoutManagerExpensive = new LinearLayoutManager(getContext());

        mRecyclerViewIngress.setLayoutManager(layoutManagerIngress);
        mRecyclerViewIngress.setItemAnimator(new DefaultItemAnimator());


        mRecyclerViewExpensive.setLayoutManager(layoutManagerExpensive);
        mRecyclerViewExpensive.setItemAnimator(new DefaultItemAnimator());



        mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),CategoryActivity.class);

                startActivityForResult(i,0);
            }
        });
        update();
        return view;
    }

    public static HomeFragment getInstance() {
        return instance;
    }


    public void update(){
        float expense=db.getTotal(2,Util.getCurrentMonth(),Util.getCurrentYear());
        float income=db.getTotal(1,Util.getCurrentMonth(),Util.getCurrentYear());
        mExpense.setText("$"+expense);
        mIncome.setText("$"+income);
        String total=String.format(java.util.Locale.US,"%.2f", (income-expense));
        mTotal.setText("$"+total);

        mCategoryMonthArrayListIngress=db.getMonthlyConcept(1, Util.getCurrentMonth(),Util.getCurrentYear());
        mAdapterIngress = new CategoryMonthListAdapter(mCategoryMonthArrayListIngress);
        mRecyclerViewIngress.setAdapter(mAdapterIngress);

        mCategoryMonthArrayListExpensive=db.getMonthlyConcept(2,Util.getCurrentMonth(),Util.getCurrentYear());
        mAdapterExpensive=new CategoryMonthListAdapter(mCategoryMonthArrayListExpensive);
        mRecyclerViewExpensive.setAdapter(mAdapterExpensive);
    }

}
