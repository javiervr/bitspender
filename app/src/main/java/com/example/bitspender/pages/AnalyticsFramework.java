package com.example.bitspender.pages;


import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.example.bitspender.MainActivity;
import com.example.bitspender.R;
import com.example.bitspender.adapter.SpinAdapter;
import com.example.bitspender.controller.DatabaseHelper;
import com.example.bitspender.model.SpinnerItem;
import com.example.bitspender.utilities.Util;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnalyticsFramework extends Fragment {

    private PieChart pieChart;
    private LineChart mLineChart;
    private DatabaseHelper db;
    private static AnalyticsFramework instance = null;
    Spinner mSpinnerMonth,mSpinnerConcept,mSpinnerYear,mSpinnerBy;
    private SpinAdapter mSpinAdapterMonth1,mSpinAdapterConcept,mSpinAdapterYear,mSpinAdapterBy;
    private int currentConcept1,year,by;
    private String currentMonth1;
    public AnalyticsFramework() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        instance=this;
        View view=inflater.inflate(R.layout.fragment_analytics_framework, container, false);
        by=1;
        year=Util.getCurrentYear();
        db=new DatabaseHelper(getContext());
        pieChart = view.findViewById(R.id.piechart);
        mLineChart=view.findViewById(R.id.linechart);
        mLineChart.setPinchZoom(true);
        mLineChart.setTouchEnabled(true);

        mSpinnerConcept=view.findViewById(R.id.spinner_concept1);
        mSpinnerMonth=view.findViewById(R.id.spinner_month);
        mSpinnerYear=view.findViewById(R.id.spinner_year);
        mSpinnerBy=view.findViewById(R.id.spinner_by);

        //DATOS CONCEPT TYPE
        ArrayList<SpinnerItem> conceptType = new ArrayList<SpinnerItem>();
        conceptType.add(new SpinnerItem(1,"Ingreso"));
        conceptType.add(new SpinnerItem(2,"Gasto"));

        //DATOS BY TYPE
        ArrayList<SpinnerItem> byFilter = new ArrayList<SpinnerItem>();
        byFilter.add(new SpinnerItem(1,"Mensual"));
        byFilter.add(new SpinnerItem(2,"Semanal"));

        //FILTER BY SPINNER
        mSpinAdapterBy=new SpinAdapter(view.getContext(),android.R.layout.simple_spinner_dropdown_item,byFilter);
        mSpinnerBy.setAdapter(mSpinAdapterBy);

        mSpinnerBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterBy.getItem(position);
                by=data.getValue();
                setLineData();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
        //YEAR SPINNER
        mSpinAdapterYear=new SpinAdapter(view.getContext(),android.R.layout.simple_spinner_dropdown_item,db.getYears());
        mSpinAdapterYear.setTxtColor(Color.WHITE);
        mSpinnerYear.setAdapter(mSpinAdapterYear);

        mSpinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterYear.getItem(position);
                year=data.getValue();

                setPieData();
                setLineData();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        mSpinnerYear.setSelection(mSpinAdapterYear.getPositionByValue(Util.getCurrentYear()));
        //MONTH SPINNER
        mSpinAdapterMonth1=new SpinAdapter(view.getContext(),android.R.layout.simple_spinner_dropdown_item,db.getMonths(Util.getCurrentYear()));
        mSpinnerMonth.setAdapter(mSpinAdapterMonth1);

        mSpinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterMonth1.getItem(position);

                currentMonth1=Util.formatMonth(data.getValue());
                System.out.println("current month--->"+currentMonth1);

                setPieData();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
        Calendar cal = Calendar.getInstance();
        int month=Integer.parseInt(new SimpleDateFormat("MM").format(cal.getTime()));
        currentConcept1=1;
        currentMonth1=Util.formatMonth(month);
        System.out.println("month--->"+month);
        System.out.println("position-->"+mSpinAdapterMonth1.getPositionByValue(month));
        mSpinnerMonth.setSelection(mSpinAdapterMonth1.getPositionByValue(month));

        //CONCEPT TYPE SPINNER
        mSpinAdapterConcept = new SpinAdapter(view.getContext(), android.R.layout.simple_spinner_dropdown_item, conceptType);
        mSpinnerConcept.setAdapter(mSpinAdapterConcept);
        mSpinnerConcept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                SpinnerItem data = mSpinAdapterConcept.getItem(position);
                currentConcept1=data.getValue();
                setPieData();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        setPieData();
        setLineData();

        return view;
    }
    public static AnalyticsFramework getInstance() {
        return instance;
    }

    public void setPieData(){
        PieDataSet dataSet = new PieDataSet(db.getPieCategory(currentConcept1,currentMonth1,year),null);
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        PieData data2 = new PieData(dataSet);
        data2.setValueTextSize(12f);
        data2.setValueTextColor(Color.WHITE);
        data2.setDrawValues(true);
        pieChart.setData(data2);
        pieChart.invalidate();
        pieChart.setDrawHoleEnabled(false);
        pieChart.animateXY(1000,1000);

    }

    public void setLineData(){
        LineDataSet lineDataSet1=new LineDataSet(db.getConceptEntry(1,year,by),"Ingreso");
        lineDataSet1.setColor(Color.GREEN);
        lineDataSet1.setCircleColor(Color.WHITE);
        lineDataSet1.setLineWidth(3f);
        LineDataSet lineDataSet2=new LineDataSet(db.getConceptEntry(2,year,by),"Gasto");
        lineDataSet2.setColor(Color.RED);
        lineDataSet2.setCircleColor(Color.WHITE);
        lineDataSet2.setLineWidth(3f);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(lineDataSet1);
        dataSets.add(lineDataSet2);
        LineData lineData=new LineData(dataSets);
        lineData.setValueTextColor(Color.BLUE);
        lineData.setValueTextSize(9f);
        mLineChart.setData(lineData);
        mLineChart.invalidate();
        mLineChart.animateXY(1000,1000);
    }

}
