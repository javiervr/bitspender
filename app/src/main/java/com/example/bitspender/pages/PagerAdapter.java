package com.example.bitspender.pages;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapter  extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment home = new HomeFragment();
                return home;
            case 1:
                HistoryFragment history=new HistoryFragment();
                return history;
            case 2:
                AnalyticsFramework analytics=new AnalyticsFramework();
                return analytics;
            case 3:
                AnalyticsFramework analytics2=new AnalyticsFramework();
                return analytics2;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }
}
