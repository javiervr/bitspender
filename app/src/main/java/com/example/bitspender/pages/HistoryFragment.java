package com.example.bitspender.pages;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bitspender.MainActivity;
import com.example.bitspender.R;
import com.example.bitspender.adapter.HistoryListAdapter;
import com.example.bitspender.adapter.SpinAdapter;
import com.example.bitspender.controller.DatabaseHelper;
import com.example.bitspender.model.HistoryModel;
import com.example.bitspender.model.SpinnerItem;
import com.example.bitspender.utilities.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private static RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private  ArrayList<HistoryModel> data;
    private static RecyclerView.Adapter adapter;
    private static DatabaseHelper db;
    private View view;
    private static HistoryFragment instance = null;
    public static View.OnClickListener myOnClickListener;

    private Spinner mSpinnerMonth,mSpinnerYear;
    private SpinAdapter mSpinAdapterMonth,mSpinAdapterYear;
    private int year;
    private String currentMonth;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myOnClickListener = new MyOnClickListener(getContext());



        instance=this;
        view=inflater.inflate(R.layout.fragment_history, container, false);
        db=new DatabaseHelper(getContext());

        mSpinnerMonth=view.findViewById(R.id.spinner_month);
        mSpinnerYear=view.findViewById(R.id.spinner_year);
        recyclerView =  view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<HistoryModel>();
        data=db.getHistory(Util.getCurrentMonth(),Util.getCurrentYear());
        adapter = new HistoryListAdapter(data);

        recyclerView.setAdapter(adapter);

        //YEAR SPINNER
        mSpinAdapterYear=new SpinAdapter(view.getContext(),android.R.layout.simple_spinner_dropdown_item,db.getYears());
        mSpinAdapterYear.setTxtColor(Color.WHITE);
        mSpinnerYear.setAdapter(mSpinAdapterYear);

        mSpinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterYear.getItem(position);
                year=data.getValue();
                setData();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        mSpinnerYear.setSelection(mSpinAdapterYear.getPositionByValue(Util.getCurrentYear()));
        //MONTH SPINNER
        mSpinAdapterMonth=new SpinAdapter(view.getContext(),android.R.layout.simple_spinner_dropdown_item,db.getMonths(Util.getCurrentYear()));
        mSpinAdapterMonth.setTxtColor(Color.WHITE);

        mSpinnerMonth.setAdapter(mSpinAdapterMonth);

        mSpinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterMonth.getItem(position);
                currentMonth=Util.formatMonth(data.getValue());
                String currentMonth1=Util.formatMonth(data.getValue());
                System.out.println("current month--->"+currentMonth1);

                setData();

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
        Calendar cal = Calendar.getInstance();
        int month=Integer.parseInt(new SimpleDateFormat("MM").format(cal.getTime()));
        System.out.println("month--->"+month);
        System.out.println("position-->"+mSpinAdapterMonth.getPositionByValue(month));
        mSpinnerMonth.setSelection(mSpinAdapterMonth.getPositionByValue(month));



        return view;
    }

    public static HistoryFragment getInstance() {
        return instance;
    }

    public void update() {
        Calendar cal = Calendar.getInstance();

        int month=Integer.parseInt(new SimpleDateFormat("MM").format(cal.getTime()));
        mSpinnerMonth.setSelection(mSpinAdapterMonth.getPositionByValue(month));
        db = new DatabaseHelper(getActivity());
        data=db.getHistory(Util.getCurrentMonth(),Util.getCurrentYear());
        adapter = new HistoryListAdapter(data);
        recyclerView.setAdapter(adapter);
    }


    public void setData(){
        data=db.getHistory(currentMonth,year);
        adapter = new HistoryListAdapter(data);
        recyclerView.setAdapter(adapter);
    }

    private static class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;

        }

        @Override
        public void onClick(View v) {
            removeItem(v);
        }

        private void removeItem(View v) {
            int selectedItemPosition = recyclerView.getChildPosition(v);
            RecyclerView.ViewHolder viewHolder
                    = recyclerView.findViewHolderForPosition(selectedItemPosition);
            TextView textViewId
                    = (TextView) viewHolder.itemView.findViewById(R.id.txt_item_id);
            int id=Integer.parseInt(textViewId.getText().toString());


            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            ViewGroup viewGroup = v.findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_history_description, viewGroup, false);
            //componentes
            ImageButton close =dialogView.findViewById(R.id.btn_close);
            ImageView icon=dialogView.findViewById(R.id.icon_history);
            TextView textViewCategory=dialogView.findViewById(R.id.text_categoria);
            TextView textViewAmount=dialogView.findViewById(R.id.amount_item_txt);
            TextView textViewDate=dialogView.findViewById(R.id.date_item_txt);
            TextView textViewDescription=dialogView.findViewById(R.id.txt_description);

            HistoryModel data=db.getConcept(id);
            textViewCategory.setText(data.getCategory());
            textViewAmount.setTextColor(Color.rgb(47,184,23));
            textViewAmount.setText("$"+data.getAmount());
            if(data.getConcept_type_fk()==2){
                textViewAmount.setTextColor(Color.RED);
                textViewAmount.setText("- $"+data.getAmount());
            }

            textViewDate.setText(data.getDate());
            if(data.getDescription().equals("")){
                textViewDescription.setText("Sin descripción");
            }else{
                textViewDescription.setText(data.getDescription());

            }
            icon.setImageResource(data.getCategoryIcon());


            builder.setView(dialogView);
            final AlertDialog alertDialog = builder.create();
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.cancel();
                }
            });


            alertDialog.show();
        }
    }
}
