package com.example.bitspender;

import androidx.appcompat.*;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.bitspender.activity.Settings;
import com.example.bitspender.adapter.CategorySpinAdapter;
import com.example.bitspender.controller.DatabaseHelper;
import com.example.bitspender.model.Category;
import com.example.bitspender.model.Concept;
import com.example.bitspender.model.ConceptType;
import com.example.bitspender.pages.AnalyticsFramework;
import com.example.bitspender.pages.HistoryFragment;
import com.example.bitspender.pages.HomeFragment;
import com.example.bitspender.pages.PagerAdapter;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;

public class MainActivity extends AppCompatActivity {
    private static final String CERO = "0";
    private static final String BARRA = "/";
    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();
    //Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    private CategorySpinAdapter mCategorySpinAdapter;
    EditText edFecha,edAmount,edDescription;
    ViewPager mViewPager;
    SpaceNavigationView mSpaceNavigationView;
    Spinner mSpinnerCategory;
    SegmentedGroup mSegmentedConceptType;
    DatabaseHelper db;
    Concept newConcept;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide(); //hide the title bar

        db=new DatabaseHelper(this);
        firstData();

        mViewPager=findViewById(R.id.pager);

        mSpaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        mSpaceNavigationView.initWithSaveInstanceState(savedInstanceState);

        mSpaceNavigationView.addSpaceItem(new SpaceItem("HomeFragment", R.drawable.ic_home_black_24dp));
        mSpaceNavigationView.addSpaceItem(new SpaceItem("History", R.drawable.ic_history_black_24dp));
        mSpaceNavigationView.addSpaceItem(new SpaceItem("Stats", R.drawable.ic_show_chart_black_24dp));
        mSpaceNavigationView.addSpaceItem(new SpaceItem("Export", R.drawable.ic_export));
        mSpaceNavigationView.showIconOnly();


        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(),3);

        mViewPager.setAdapter(adapter);

        mSpaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                dialogAdd();
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {

                if(itemIndex==3){
                    Intent intent = new Intent(MainActivity.this, Settings.class);
                    startActivity(intent);
                }else{
                    mViewPager.setCurrentItem(itemIndex);
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void firstData(){

        if(db.countConceptType()==0){
            db.addConcepType(new ConceptType(1,"Ingreso"));
            db.addConcepType(new ConceptType(2,"Gasto"));
        }

        if(db.countCategories()==0){
            db.addCategory(new Category(1,"Alimento", R.drawable.ic_food,true,2));
            db.addCategory(new Category(2,"Gasolina",R.drawable.ic_gas,true,2));
            db.addCategory(new Category(3,"Salud",R.drawable.ic_health,true,2));
            db.addCategory(new Category(4,"Diversión",R.drawable.ic_fun,true,2));
            db.addCategory(new Category(5,"Impuesto",R.drawable.ic_tax,true,2));
            db.addCategory(new Category(6,"Proovedores",R.drawable.ic_provider,true,2));
            db.addCategory(new Category(7,"Hipoteca",R.drawable.ic_rent,true,2));
            db.addCategory(new Category(8,"Renta",R.drawable.ic_rent,true,2));


            db.addCategory(new Category(9,"Banco",R.drawable.ic_bank,true,1));
            db.addCategory(new Category(10,"Venta",R.drawable.ic_sold,true,1));
            db.addCategory(new Category(11,"Renta",R.drawable.ic_rent,true,1));

        }
    }

    public void dialogAdd(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getBaseContext()).inflate(R.layout.dialog_add, viewGroup, false);
        newConcept=new Concept();
        ImageButton done =dialogView.findViewById(R.id.done);
        ImageButton close =dialogView.findViewById(R.id.btn_close);
        edFecha=dialogView.findViewById(R.id.edit_date);
        String currentDate = new SimpleDateFormat("yyy-MM-dd", Locale.getDefault()).format(new Date());

        edFecha.setText(currentDate);
        edAmount=dialogView.findViewById(R.id.edit_amount);
        edDescription=dialogView.findViewById(R.id.edit_description);

        mSpinnerCategory=dialogView.findViewById(R.id.spinner_category);
        mSegmentedConceptType =dialogView.findViewById(R.id.segmented_concept_type);
        List<Category> categories = db.getAllCategoriesByConcept(1);
        mCategorySpinAdapter = new CategorySpinAdapter(getBaseContext(),
                android.R.layout.simple_spinner_dropdown_item,
                categories);

        newConcept.setDate(currentDate);//colocamos fecha actual por default
        newConcept.setConcept_type_fk(1);//colocamos ingreso por default
        newConcept.setCategory_fk(categories.get(0).getId());
        mSpinnerCategory.setAdapter(mCategorySpinAdapter);
        mSegmentedConceptType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                List<Category> categories =null;
                switch (checkedId) {
                    case R.id.button21:
                        Toast.makeText(getBaseContext(), "Ingreso", Toast.LENGTH_SHORT).show();
                        newConcept.setConcept_type_fk(1);
                        categories =db.getAllCategoriesByConcept(1);
                        newConcept.setCategory_fk(categories.get(0).getId());
                        break;
                    case R.id.button22:
                        Toast.makeText(getBaseContext(), "Gasto", Toast.LENGTH_SHORT).show();
                        newConcept.setConcept_type_fk(2);
                        categories =db.getAllCategoriesByConcept(2);
                        newConcept.setCategory_fk(categories.get(0).getId());
                        break;
                }
                mCategorySpinAdapter = new CategorySpinAdapter(getBaseContext(),
                        android.R.layout.simple_spinner_dropdown_item,
                        categories);
                mSpinnerCategory.setAdapter(mCategorySpinAdapter);
            }
        });



        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        mSpinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                Category cat = mCategorySpinAdapter.getItem(position);

                newConcept.setCategory_fk(cat.getId());

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edAmount.getText().toString().isEmpty()){
                    edAmount.requestFocus();
                    return;
                }

                newConcept.setAmount(Float.valueOf(edAmount.getText().toString()));
                newConcept.setDescription(edDescription.getText().toString());

                if(db.addConcept(newConcept)){
                    alertDialog.cancel();


                    if(HomeFragment.getInstance()!=null){
                        HomeFragment.getInstance().update();
                    }
                    if(AnalyticsFramework.getInstance()!=null){
                        AnalyticsFramework.getInstance().setLineData();
                    }

                    if(HistoryFragment.getInstance()!=null){
                        HistoryFragment.getInstance().update();
                    }


                }else{
                    Toast.makeText(MainActivity.this,"Ocurrio un error", Toast.LENGTH_SHORT).show();
                    alertDialog.cancel();
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        edFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFecha();
            }
        });

        alertDialog.show();

    }
    private void obtenerFecha(){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                edFecha.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
                newConcept.setDate(year+"-"+mesFormateado+"-"+diaFormateado);

            }

        },anio, mes, dia);
        recogerFecha.show();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mSpaceNavigationView.onSaveInstanceState(outState);
    }
}
