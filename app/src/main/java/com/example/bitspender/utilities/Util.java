package com.example.bitspender.utilities;

import java.util.Calendar;

public class Util {


    public static String getCurrentMonth(){
        Calendar c = Calendar.getInstance();
        int M = c.get(Calendar.MONTH);
        M++;
        String month=M>9?M+"":"0"+M;
        return month;
    }

    public static String formatMonth(int M){
        String month=M>9?M+"":"0"+M;
        return month;
    }

    public static int getCurrentYear(){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        return year;
    }
}
