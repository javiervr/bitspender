package com.example.bitspender.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bitspender.R;
import com.example.bitspender.model.CategoryMonth;
import com.example.bitspender.model.HistoryModel;

import java.util.ArrayList;

public class CategoryMonthListAdapter extends RecyclerView.Adapter<CategoryMonthListAdapter.MyViewHolder>{

    private ArrayList<CategoryMonth> dataSet;

    public CategoryMonthListAdapter(ArrayList<CategoryMonth> data) {
        this.dataSet = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_month, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TextView textViewAmount = holder.textAmount;
        TextView textViewName = holder.textCategoryName;
        ImageView imageView = holder.imageViewIcon;


        if(dataSet.get(position).getConcept_type_fk()==1){
            textViewAmount.setTextColor(Color.rgb(47,184,23));
            textViewAmount.setText("$"+dataSet.get(position).getAmount());
        }else{
            textViewAmount.setTextColor(Color.RED);
            textViewAmount.setText("$"+dataSet.get(position).getAmount());
        }

        textViewName.setText(dataSet.get(position).getName());

        imageView.setImageResource(dataSet.get(position).getIcon());
        //int resourceImage = holder.getResources().getIdentifier(dataSet.get(position).getDescription(), "drawable", HistoryFragment.getPackageName());
        //image.setImageResource(resourceImage);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textAmount;
        TextView textCategoryName;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textAmount =  itemView.findViewById(R.id.amount_item_txt);
            this.textCategoryName =  itemView.findViewById(R.id.category_item_txt);
            this.imageViewIcon =  itemView.findViewById(R.id.icon_item);
        }
    }

}
