package com.example.bitspender.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bitspender.R;
import com.example.bitspender.model.Category;
import com.example.bitspender.model.HistoryModel;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder>{

    private ArrayList<Category> dataSet;

    public CategoryListAdapter(ArrayList<Category> data) {
        this.dataSet = data;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        CategoryListAdapter.MyViewHolder myViewHolder = new CategoryListAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListAdapter.MyViewHolder holder, int position) {
        TextView textViewName = holder.mTextViewName;
        EditText editTextLimit = holder.mEditTextLimit;
        ImageView imageView = holder.mImageViewIcon;


        textViewName.setText(dataSet.get(position).getName());
        editTextLimit.setText("1");
        imageView.setImageResource(dataSet.get(position).getIcono());
        //int resourceImage = holder.getResources().getIdentifier(dataSet.get(position).getDescription(), "drawable", HistoryFragment.getPackageName());
        //image.setImageResource(resourceImage);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mTextViewName;
        EditText mEditTextLimit;
        ImageView mImageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.mTextViewName =  itemView.findViewById(R.id.category_name_txt);
            this.mEditTextLimit =  itemView.findViewById(R.id.limit_editText);
            this.mImageViewIcon =  itemView.findViewById(R.id.category_icon);
        }
    }
}
