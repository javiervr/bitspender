package com.example.bitspender.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bitspender.model.SpinnerItem;

import java.util.List;

public class SpinAdapterMonth extends ArrayAdapter<SpinnerItem> {
    private Context mContext;
    private List<SpinnerItem> values;
    private int txtColor;

    public SpinAdapterMonth(Context context, int textViewResourceId, List<SpinnerItem> values) {
        super(context, textViewResourceId, values);
        this.mContext=context;
        this.values=values;
        txtColor=Color.BLACK;
    }

    public int getPositionByValue(int value){
        int pos=0;
        for(SpinnerItem item:values){
                if(item.getValue()==value)
                    break;

                pos++;
        }
        return pos;
    }

    public void setTxtColor(int txtColor){
        this.txtColor=txtColor;
    }
    @Override
    public int getCount() {
        return values.size();
    }
    @Override
    public SpinnerItem getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(txtColor);
        label.setText(values.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getName());

        return label;
    }


}
