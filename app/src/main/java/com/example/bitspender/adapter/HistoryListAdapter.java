package com.example.bitspender.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bitspender.R;
import com.example.bitspender.model.HistoryModel;
import com.example.bitspender.pages.HistoryFragment;

import java.util.ArrayList;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.MyViewHolder>{

    private ArrayList<HistoryModel> dataSet;

    public HistoryListAdapter(ArrayList<HistoryModel> data) {
        this.dataSet = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);

        view.setOnClickListener(HistoryFragment.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    public HistoryModel getItemByPosition(int position){
        return dataSet.get(position);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TextView textViewAmount = holder.textAmount;
        TextView textViewDate = holder.textDate;
        TextView textViewId = holder.textId;
        ImageView imageView = holder.imageViewIcon;

        if(dataSet.get(position).getConcept_type_fk()==1){
            textViewAmount.setTextColor(Color.rgb(47,184,23));
            textViewAmount.setText("$"+dataSet.get(position).getAmount());
        }else{
            textViewAmount.setTextColor(Color.RED);
            textViewAmount.setText("- $"+dataSet.get(position).getAmount());
        }

        textViewDate.setText(dataSet.get(position).getDate());
        textViewId.setText(dataSet.get(position).getId()+"");

        imageView.setImageResource(dataSet.get(position).getCategoryIcon());
        //int resourceImage = holder.getResources().getIdentifier(dataSet.get(position).getDescription(), "drawable", HistoryFragment.getPackageName());
        //image.setImageResource(resourceImage);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textAmount;
        TextView textDate;
        TextView textId;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textAmount = (TextView) itemView.findViewById(R.id.amount_item_txt);
            this.textDate = (TextView) itemView.findViewById(R.id.date_item_txt);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.icon_history);
            this.textId=(TextView) itemView.findViewById(R.id.txt_item_id);
        }


    }

}
