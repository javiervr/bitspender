package com.example.bitspender.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.example.bitspender.adapter.SpinAdapter;
import com.example.bitspender.controller.DatabaseHelper;
import com.example.bitspender.model.SpinnerItem;
import com.example.bitspender.utilities.Util;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.bitspender.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Settings extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private static final int PERMISSION_REQUEST_CODE = 1;

    private Spinner mSpinnerMonth,mSpinnerYear;
    private SpinAdapter mSpinAdapterMonth,mSpinAdapterYear;
    private int year;
    private String currentMonth;


    Button mButton;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Exportar");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mButton=findViewById(R.id.btn_export_csv);
        db=new DatabaseHelper(this);
        mSpinnerMonth=findViewById(R.id.spinner_month);
        mSpinnerYear=findViewById(R.id.spinner_year);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("exportar");


                db.exportCSV(getBaseContext(),year,currentMonth);

            }
        });

        //YEAR SPINNER
        mSpinAdapterYear=new SpinAdapter(this,android.R.layout.simple_spinner_dropdown_item,db.getYears());
        mSpinnerYear.setAdapter(mSpinAdapterYear);

        mSpinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterYear.getItem(position);
                year=data.getValue();


            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        mSpinnerYear.setSelection(mSpinAdapterYear.getPositionByValue(Util.getCurrentYear()));
        //MONTH SPINNER
        mSpinAdapterMonth=new SpinAdapter(this,android.R.layout.simple_spinner_dropdown_item,db.getMonths(Util.getCurrentYear()));

        mSpinnerMonth.setAdapter(mSpinAdapterMonth);

        mSpinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                SpinnerItem data = mSpinAdapterMonth.getItem(position);
                currentMonth=Util.formatMonth(data.getValue());


            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
        Calendar cal = Calendar.getInstance();
        int month=Integer.parseInt(new SimpleDateFormat("MM").format(cal.getTime()));

        mSpinnerMonth.setSelection(mSpinAdapterMonth.getPositionByValue(month));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });


    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }





}
