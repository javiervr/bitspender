package com.example.bitspender.controller;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.example.bitspender.model.Category;
import com.example.bitspender.model.CategoryMonth;
import com.example.bitspender.model.ChartModel;
import com.example.bitspender.model.Concept;
import com.example.bitspender.model.ConceptType;
import com.example.bitspender.model.HistoryModel;
import com.example.bitspender.model.SpinnerItem;
import com.example.bitspender.model.SpinnerItemMonth;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;


public class DatabaseHelper extends SQLiteOpenHelper{
    //Version
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="bitSpender";
    //Tables
    private static final String TABLE_CONCEPT="concept";
    private static final String TABLE_CONCEPT_TYPE="concept_type";
    private static final String TABLE_CATEGORY="category";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    //Creando tabla
    @Override
    public void onCreate(SQLiteDatabase db) {
        //CONCEPT_TYPE TABLE
        String CREATE_CONCEPT_TYPE_TABLE="CREATE TABLE "+TABLE_CONCEPT_TYPE+"(concept_type_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT)";
        db.execSQL(CREATE_CONCEPT_TYPE_TABLE);

        //TABLE CATEGORY
        String CREATE_CATEGORY_TABLE="CREATE TABLE "+TABLE_CATEGORY+"(category_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,icono TEXT,status BOOLEAN,concept_type_fk INTEGER,CONSTRAINT concept_typeFK FOREIGN KEY (concept_type_fk) REFERENCES "+TABLE_CONCEPT_TYPE+"(concept_type_id))";
        db.execSQL(CREATE_CATEGORY_TABLE);

        //TABLE CONCEPT
        String CREATE_CONCEPT_TABLE ="CREATE TABLE "+TABLE_CONCEPT+"(concept_id INTEGER PRIMARY KEY AUTOINCREMENT,amount DECIMAL(10,5),description TEXT,date DATE,category_fk INTEGER,concept_type_fk INTEGER,CONSTRAINT categoryFK FOREIGN KEY (category_fk) REFERENCES "+TABLE_CATEGORY+"(category_id),CONSTRAINT concept_typeFK FOREIGN KEY (concept_type_fk) REFERENCES "+TABLE_CONCEPT_TYPE+"(concept_type_id) )";
        db.execSQL(CREATE_CONCEPT_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONCEPT_TYPE);
        //onCreate(db);
    }

    public void deleteTable(){
        SQLiteDatabase db=this.getReadableDatabase();
        db.delete(TABLE_CONCEPT_TYPE,null,null);
    }

    //ADD CONCEPT TYPE
    public void addConcepType(ConceptType ct){
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("name",ct.getName());
        db.insert(TABLE_CONCEPT_TYPE,null,values);
        db.close();
    }

    //ADD CATEGORY
    public void addCategory(Category cat){
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values=new ContentValues();

        values.put("name",cat.getName());
        values.put("icono",cat.getIcono());
        values.put("status",cat.isStatus());
        values.put("concept_type_fk",cat.getConcept_type_fk());

        db.insert(TABLE_CATEGORY,null,values);
        db.close();
    }

    //ADD CONCEPT
    public boolean addConcept(Concept con){
        boolean ret=false;
        try {
            SQLiteDatabase db =this.getWritableDatabase();
            ContentValues values=new ContentValues();

            values.put("amount",con.getAmount());
            values.put("description",con.getDescription());
            values.put("date",con.getDate());
            values.put("category_fk",con.getCategory_fk());
            values.put("concept_type_fk",con.getConcept_type_fk());

            db.insert(TABLE_CONCEPT,null,values);
            db.close();

            ret= true;

        }catch (SQLiteException ex){
            System.out.println(ex);
        }
        return  ret;
    }

    //GET ALL CONCEPT TYPES
    public List<ConceptType> getAllConcepTypes() {
        ArrayList<ConceptType> allConcepts = new ArrayList<ConceptType>();
        String selectQuery = "SELECT * FROM " + TABLE_CONCEPT_TYPE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ConceptType concept = new ConceptType();
                concept.setId(Integer.parseInt(cursor.getString(0)));
                concept.setName(cursor.getString(1));
                allConcepts.add(concept);
            } while (cursor.moveToNext());
        }
        db.close();
        return allConcepts;
    }

    //GET ALL CONCEPTS
    public List<Concept> getAllConceps() {
        ArrayList<Concept> allConcepts = new ArrayList<Concept>();
        String selectQuery = "SELECT * FROM " + TABLE_CONCEPT+" ORDER BY date ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Concept concept = new Concept();
                concept.setId(Integer.parseInt(cursor.getString(0)));
                concept.setAmount(cursor.getFloat(1));
                concept.setDescription(cursor.getString(2));
                concept.setDate(cursor.getString(3));
                concept.setCategory_fk(cursor.getInt(4));
                concept.setConcept_type_fk(cursor.getInt(5));

                allConcepts.add(concept);
            } while (cursor.moveToNext());
        }
        db.close();
        return allConcepts;
    }

    //GET ALL CONCEPTS
    public ArrayList<HistoryModel> getHistory(String month,int year) {
        ArrayList<HistoryModel> allConcepts = new ArrayList<HistoryModel>();
        String selectQuery = "SELECT CON.concept_id,CON.amount,CON.description,CON.date,CAT.name,CAT.icono,CON.concept_type_fk" +
                " FROM " + TABLE_CONCEPT+" CON INNER JOIN "+TABLE_CATEGORY+" CAT ON CON.category_fk=CAT.category_id " +
                "  WHERE  strftime('%Y',CON.date)='"+year+"' and strftime('%m',CON.date)='"+month+"' " +
                "ORDER BY CON.date DESC, CON.concept_id DESC";
        System.out.println(selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HistoryModel concept = new HistoryModel();
                concept.setId(Integer.parseInt(cursor.getString(0)));
                concept.setAmount(cursor.getFloat(1));
                concept.setDescription(cursor.getString(2));
                concept.setDate(cursor.getString(3));
                concept.setCategory(cursor.getString(4));
                concept.setCategoryIcon(cursor.getInt(5));
                concept.setConcept_type_fk(cursor.getInt(6));

                allConcepts.add(concept);
            } while (cursor.moveToNext());
        }
        db.close();
        return allConcepts;
    }

    //GET ALL CONCEPTS
    public HistoryModel getConcept(int id) {
        String selectQuery = "SELECT CON.concept_id,CON.amount,CON.description,CON.date,CAT.name,CAT.icono,CON.concept_type_fk" +
                " FROM " + TABLE_CONCEPT+" CON INNER JOIN "+TABLE_CATEGORY+" CAT ON CON.category_fk=CAT.category_id " +
                "  WHERE  CON.concept_id="+id;

        System.out.println(selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            HistoryModel concept = new HistoryModel();
            concept.setId(Integer.parseInt(cursor.getString(0)));
            concept.setAmount(cursor.getFloat(1));
            concept.setDescription(cursor.getString(2));
            concept.setDate(cursor.getString(3));
            concept.setCategory(cursor.getString(4));
            concept.setCategoryIcon(cursor.getInt(5));
            concept.setConcept_type_fk(cursor.getInt(6));
            db.close();
            return concept;

        }

        db.close();
        return null;
    }
    //GET ALL CATEGORIES
    public List<Category> getAllCategories() {
        ArrayList<Category> allCategories = new ArrayList<Category>();
        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Category category = new Category();
                category.setId(Integer.parseInt(cursor.getString(0)));
                category.setName(cursor.getString(1));
                category.setIcono(cursor.getInt(2));
                category.setStatus(cursor.getInt(3) > 0);
                category.setConcept_type_fk(cursor.getInt(4));

                allCategories.add(category);
            } while (cursor.moveToNext());
        }
        return allCategories;
    }
    //OBTENER CONTACTO
    public ArrayList<Category> getAllCategoriesByConcept(int concept_id){
        ArrayList<Category> allCategories = new ArrayList<Category>();
        SQLiteDatabase db =this.getReadableDatabase();
        Cursor cursor=db.query(
                TABLE_CATEGORY,
                new String[]{"category_id","name","icono","status","concept_type_fk"},
                "concept_type_fk=? and status=?",
                new String[]{String.valueOf(concept_id),"1"},
                null,
                null,
                "name",
                null
        );

        if (cursor.moveToFirst()) {
            do {
                Category category = new Category();
                category.setId(Integer.parseInt(cursor.getString(0)));
                category.setName(cursor.getString(1));
                category.setIcono(cursor.getInt(2));
                category.setStatus(cursor.getInt(3) > 0);
                category.setConcept_type_fk(cursor.getInt(4));
                allCategories.add(category);
            } while (cursor.moveToNext());
        }
        return allCategories;
    }

    //GET CATEGORIES COUNT
    public int countCategories(){
        String countQuery="SELECT * FROM "+TABLE_CATEGORY;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery,null);
        return cursor.getCount();

    }

    //GET CONCEPT TYPE COUNT
    public int countConceptType(){
        String countQuery="SELECT * FROM "+TABLE_CONCEPT_TYPE;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery,null);

        return cursor.getCount();

    }

    //GET EXPENSE
    public float getExpense(){
        String countQuery="SELECT SUM(amount) as total FROM "+TABLE_CONCEPT+" WHERE concept_type_fk=2";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery,null);
        if (cursor.moveToFirst()) {
            float total = cursor.getInt(cursor.getColumnIndex("total"));
            db.close();
            return total;
        }
        db.close();
        return 0;

    }

    //GET CATEGORY MONTH
    public ArrayList<CategoryMonth> getMonthlyConcept(int concept_type,String month,int year){
        ArrayList<CategoryMonth> allData=new ArrayList<CategoryMonth>();

        String selectQuery = "SELECT SUM(CON.amount) as total,CAT.name,CAT.icono,CON.concept_type_fk FROM " + TABLE_CONCEPT+" CON INNER JOIN" +
                " "+TABLE_CATEGORY+" CAT ON CON.category_fk=CAT.category_id where CON.concept_type_fk="+concept_type+" and" +
                " strftime('%Y',date)='"+year+"' and strftime('%m',date)='"+month+"' GROUP BY CAT.name";
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()) {
            do {
                CategoryMonth data = new CategoryMonth();
                data.setAmount(cursor.getFloat(0));
                data.setName(cursor.getString(1));
                data.setIcon(cursor.getInt(2));
                data.setConcept_type_fk(cursor.getInt(3));
                allData.add(data);
            } while (cursor.moveToNext());
        }
        return allData;

    }

    //GET TOTAL
    public float getTotal(int concept,String month,int year){
        String countQuery="SELECT SUM(amount) as total FROM "+TABLE_CONCEPT+" WHERE concept_type_fk="+concept+" and strftime('%m',date)='"+month+"' and strftime('%Y', date)='"+year+"'";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(countQuery,null);
        if (cursor.moveToFirst()) {
            float total = cursor.getFloat(cursor.getColumnIndex("total"));
            return total;
        }
        return 0;

    }

    //GET CHART INGRESS
    public List<ChartModel> getChartIngress(){
        ArrayList<ChartModel> allData = new ArrayList<ChartModel>();

        String selectQuery="SELECT SUM(amount) as amount,DATE_FORMAT(date, \'%m-%Y\') as month FROM "+TABLE_CONCEPT+" WHERE concept_type_fk=1 group by DATE_FORMAT(date, \'%m-%Y\')";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ChartModel data = new ChartModel(
                        cursor.getString(cursor.getColumnIndex("label")),
                        cursor.getFloat(cursor.getColumnIndex("amount"))
                );
                allData.add(data);
            } while (cursor.moveToNext());
        }
        db.close();
        return allData;

    }

    //GET CHART INGRESS
    public List<Entry> getConceptEntry(int concept_type,int year,int by){
        ArrayList<Entry> dataset = new ArrayList<Entry>();

        String selectQuery="SELECT SUM(amount) as amount,strftime('%m', date) as label FROM "+TABLE_CONCEPT+" WHERE concept_type_fk="+concept_type+" and strftime('%Y',date)='"+year+"'  group by strftime('%m', date)";

        if(by==2){
            selectQuery="SELECT SUM(amount) as amount,strftime('%W', date) as label FROM "+TABLE_CONCEPT+" WHERE concept_type_fk="+concept_type+" and strftime('%Y',date)='"+year+"'  group by strftime('%W', date)";
        }
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                dataset.add(new Entry(
                        cursor.getFloat(cursor.getColumnIndex("label")),
                        cursor.getFloat(cursor.getColumnIndex("amount"))
                ));
            } while (cursor.moveToNext());
        }
        db.close();
        return dataset;

    }

    //GET PIE CATEGORY
    public List<PieEntry> getPieCategory(int concept_type, String month,int year){


        ArrayList<PieEntry> yvalues = new ArrayList<PieEntry>();

        String selectQuery="SELECT SUM(amount) as amount,cat.name as label FROM "+TABLE_CONCEPT+" con inner join "+TABLE_CATEGORY+" cat on cat.category_id=con.category_fk" +
                "  WHERE con.concept_type_fk="+concept_type+" and strftime('%m', con.date)='"+month+"' and strftime('%Y', con.date)='"+year+"' group by con.category_fk";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int cont=0;
        if (cursor.moveToFirst()) {
            do {
                float total=getTotal(concept_type,month,year);

                float amount=cursor.getFloat(cursor.getColumnIndex("amount"));
                float average=amount*100/total;

                yvalues.add(new PieEntry(
                        average, cursor.getString(cursor.getColumnIndex("label")),cont ));
                cont++;
            } while (cursor.moveToNext());
        }
        db.close();
        return yvalues;

    }


    //GET MONTHS
    public List<SpinnerItem> getMonths(int year){
        ArrayList<SpinnerItem> items = new ArrayList<SpinnerItem>();

        String selectQuery="select case strftime('%m', date) when '01' then 'Enero' when '02' then 'Febrero' when '03' then 'Marzo' when '04' then 'Abril' when '05' then 'Mayo' when '06' then 'Junio' when '07' then 'Julio' when '08' then 'Agosto' when '09' then 'Septiembre' when '10' then 'Octubre' when '11' then 'Noviembre' when '12' then 'Diciembre' else '' end\n" +
                "as name,strftime('%m', date) as value,strftime('%Y',date) as year FROM "+TABLE_CONCEPT+" WHERE year='"+year+"' group by strftime('%m', date)";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                System.out.println("num:"+ cursor.getString(cursor.getColumnIndex("value"))+" nombre:"+cursor.getString(cursor.getColumnIndex("name")));
                items.add(new SpinnerItem(
                        Integer.parseInt(cursor.getString(cursor.getColumnIndex("value"))),
                        cursor.getString(cursor.getColumnIndex("name"))
                ));
            } while (cursor.moveToNext());
        }
        db.close();
        return items;

    }

    //GET YEAR
    public List<SpinnerItem> getYears(){
        ArrayList<SpinnerItem> items = new ArrayList<SpinnerItem>();

        String selectQuery="select strftime('%Y',date) as year FROM "+TABLE_CONCEPT+"  group by strftime('%Y', date)";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                items.add(new SpinnerItem(
                        cursor.getInt(cursor.getColumnIndex("year")),
                        cursor.getString(cursor.getColumnIndex("year"))
                ));
            } while (cursor.moveToNext());
        }
        db.close();
        return items;

    }


    public boolean exportDatabase() {
        int year=2019;
        String month="11";
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
        /**First of all we check if the external storage of the device is available for writing.
         * Remember that the external storage is not necessarily the sd card. Very often it is
         * the device storage.
         */
        String state = Environment.getExternalStorageState();

            //We use the Download directory for saving our .csv file.
            

        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists())
        {
            exportDir.mkdirs();
        }

            File file;
            PrintWriter printWriter = null;
            try
            {
                file = new File(exportDir, "MyCSVFile.csv");
                file.createNewFile();
                printWriter = new PrintWriter(new FileWriter(file));

                /**This is our database connector class that reads the data from the database.
                 * The code of this class is omitted for brevity.
                 */
                SQLiteDatabase db=this.getReadableDatabase();

                String selectQuery = "SELECT CON.concept_id,CON.amount,CON.description,CON.date,CAT.name,CAT.icono,CON.concept_type_fk" +
                        " FROM " + TABLE_CONCEPT+" CON INNER JOIN "+TABLE_CATEGORY+" CAT ON CON.category_fk=CAT.category_id " +
                        "  WHERE  strftime('%Y',CON.date)='"+year+"' and strftime('%m',CON.date)='"+month+"' " +
                        "ORDER BY CON.date DESC, CON.concept_id DESC";
                System.out.println(selectQuery);


                Cursor curCSV = db.rawQuery(selectQuery, null);
                //Write the name of the table and the name of the columns (comma separated values) in the .csv file.
                printWriter.println("FIRST TABLE OF THE DATABASE");
                printWriter.println("TIPO,CATEGORIA,DESCRIPCIÓN,FECHA,MONTO");
                while(curCSV.moveToNext())
                {
                    String categoryName=curCSV.getString(curCSV.getColumnIndex("name"));
                    String description = curCSV.getString(curCSV.getColumnIndex("description"));
                    String date = curCSV.getString(curCSV.getColumnIndex("date"));
                    Double amount = curCSV.getDouble(curCSV.getColumnIndex("amount"));
                    String concept_type = curCSV.getInt(curCSV.getColumnIndex("concept_type_fk"))==1?"Ingreso":"Gasto";

                    String record = concept_type+ "," + categoryName + "," +description+","+date+",$"+ amount;
                    printWriter.println(record); //write the record in the .csv file
                }

                curCSV.close();
                db.close();
            }

            catch(Exception exc) {
                System.out.println(exc);
                //if there are any exceptions, return false
                return false;
            }
            finally {
                if(printWriter != null) printWriter.close();
            }

            //If there are no errors, return true.
            return true;

    }

    public void exportCSV(Context context,int year,String month){
        StringBuilder data = new StringBuilder();

        String fileName="Reporte del mes:"+month+" ano:"+year;
        try{
            SQLiteDatabase db=this.getReadableDatabase();

            String selectQuery = "SELECT CON.concept_id,CON.amount,CON.description,CON.date,CAT.name,CAT.icono,CON.concept_type_fk" +
                    " FROM " + TABLE_CONCEPT+" CON INNER JOIN "+TABLE_CATEGORY+" CAT ON CON.category_fk=CAT.category_id " +
                    "  WHERE  strftime('%Y',CON.date)='"+year+"' and strftime('%m',CON.date)='"+month+"' " +
                    "ORDER BY CON.date DESC, CON.concept_id DESC";
            System.out.println(selectQuery);
            Cursor curCSV = db.rawQuery(selectQuery, null);
            data.append(fileName);

            data.append("\n"+"TIPO,CATEGORIA,DESCRIPCIÓN,FECHA,MONTO");
            while(curCSV.moveToNext())
            {
                String categoryName=curCSV.getString(curCSV.getColumnIndex("name"));
                String description = curCSV.getString(curCSV.getColumnIndex("description"));
                String date = curCSV.getString(curCSV.getColumnIndex("date"));
                Double amount = curCSV.getDouble(curCSV.getColumnIndex("amount"));
                String concept_type = curCSV.getInt(curCSV.getColumnIndex("concept_type_fk"))==1?"Ingreso":"Gasto";

                String record = "\n"+concept_type+ "," + categoryName + "," +description+","+date+",$"+ amount;
                data.append(record);
            }

            curCSV.close();
            db.close();
        } catch(Exception exc) {
            System.out.println(exc);
        }

        try{
            //saving the file into device
            FileOutputStream out = context.openFileOutput(fileName+".csv", Context.MODE_PRIVATE);
            out.write((data.toString()).getBytes());
            out.close();

            //exporting
            File filelocation = new File(context.getFilesDir(), fileName+".csv");
            Uri path = FileProvider.getUriForFile(context, "com.example.bitspender.fileprovider", filelocation);
            Intent fileIntent = new Intent(Intent.ACTION_SEND);
            fileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            fileIntent.setType("text/csv");
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, fileName);
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            fileIntent.putExtra(Intent.EXTRA_STREAM, path);
            Intent chooserIntent = Intent.createChooser(fileIntent, "Exportar");
            chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(chooserIntent);
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public void exportXSS(Context context){
        int year=2019;
        String month="10";
        String fileName="Reporte del mes:"+month+" ano:"+year;
        try {
            WritableFont titleFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, true);
            WritableCellFormat titleformat = new WritableCellFormat(titleFont);
            WritableWorkbook workbook = Workbook.createWorkbook(new File(fileName));
            WritableSheet sheet = workbook.createSheet("TLD Expand", 0);
            sheet.addCell(new Label(0, 0, "Domain name", titleformat));
            sheet.addCell(new Label(1, 0, "Name server", titleformat));
            sheet.addCell(new Label(2, 0, "Admin name", titleformat));
            sheet.addCell(new Label(3, 0, "Registrant", titleformat));
            sheet.addCell(new Label(4, 0, "Registrant", titleformat));


            SQLiteDatabase db=this.getReadableDatabase();

            String selectQuery = "SELECT CON.concept_id,CON.amount,CON.description,CON.date,CAT.name,CAT.icono,CON.concept_type_fk" +
                    " FROM " + TABLE_CONCEPT+" CON INNER JOIN "+TABLE_CATEGORY+" CAT ON CON.category_fk=CAT.category_id " +
                    "  WHERE  strftime('%Y',CON.date)='"+year+"' and strftime('%m',CON.date)='"+month+"' " +
                    "ORDER BY CON.date DESC, CON.concept_id DESC";
            Cursor curCSV = db.rawQuery(selectQuery, null);
            int nextRow = 1;
            while(curCSV.moveToNext())
            {
                String categoryName=curCSV.getString(curCSV.getColumnIndex("name"));
                String description = curCSV.getString(curCSV.getColumnIndex("description"));
                String date = curCSV.getString(curCSV.getColumnIndex("date"));
                Double amount = curCSV.getDouble(curCSV.getColumnIndex("amount"));
                String concept_type = curCSV.getInt(curCSV.getColumnIndex("concept_type_fk"))==1?"Ingreso":"Gasto";

                sheet.addCell(new Label(0, nextRow, categoryName));
                sheet.addCell(new Label(1, nextRow, description));
                sheet.addCell(new Label(2, nextRow, date));
                sheet.addCell(new Label(3, nextRow, amount+""));
                sheet.addCell(new Label(4, nextRow, concept_type));
                nextRow++;
            }

            workbook.write();
            workbook.close();
        } catch (WriteException ex) {
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
